TrackMate-export-filter
-----------------------

Instead of exporting all the tracks, export only the ones that are more than half a pixel away from the edge of the image.

So far, the filtering distance is hardcoded (look for the `thr` variable in `ExportFilteredTracksToXML.java`) to 0.5 px.

The goal is to provide a workaround for this TrackMate issue: https://github.com/fiji/TrackMate/issues/110
