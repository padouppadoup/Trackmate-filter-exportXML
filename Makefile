all:
	mvn install

clean:
	rm target/*.jar
	rm ~/.Fiji.app/plugins/TrackMate_filterExport*.jar

install :
	cp target/TrackMate_filterExport-*.jar ~/.Fiji.app/plugins/

